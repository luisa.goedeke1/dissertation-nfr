# Dissertation Nfr

Die hier hinterlegten Daten beinhalten die Analysen und Analyseergebnisse, inklusive des Codes, der Kapitel 4 und 5. Dieses Repositorium ist zunächst für die Zeit der Begutachtung gedacht. Für die Veröffentlichung soll ein Repositorium auf der Plattform Zenodo erstellt werden, damit die Daten auch dann noch einsehbar und herunterladbar sind, wenn die Autorin keinen institutionellen Zugang mehr zu Gitlab haben sollte. 

Bei Fragen kontaktieren Sie bitte luisa.goedeke@stud.uni-goettingen.de. Danke!
